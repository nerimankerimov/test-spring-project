package com.example.testapplication.service;

import com.example.testapplication.model.dto.UserDto;
import com.example.testapplication.model.entity.User;
import com.example.testapplication.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public UserDto getUserById(Integer id) {
        return userRepository.findById(id)
                .map(m -> new UserDto(m.getId(), m.getName(), m.getSurname()))
                .orElse(new UserDto());
    }

    public void createUser(User user) {
        userRepository.save(user);
    }
}
